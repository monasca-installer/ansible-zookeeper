---
# Copyright 2017 FUJITSU LIMITED

- group: name='{{ zookeeper_group }}' system=yes
- user: name='{{ zookeeper_user }}' group='{{ zookeeper_group }}' system=yes

- name: Install OpenJDK
  package: name={{ zookeeper_java_package_name }} state=present

- name: Create if not exists and change ownership of monasca download directory.
  file: path="{{ zookeeper_download_dir }}"
        state=directory
        owner="{{ zookeeper_user }}"
        group="{{ zookeeper_group }}"
        mode="u=rwX,g=rX,o="
        recurse=yes

- name: Create if not exists and change ownership of Zookeeper directory.
  file: path="{{ zookeeper_dir }}"
        state=directory
        owner="{{ zookeeper_user }}"
        group="{{ zookeeper_group }}"
        mode="u=rwX,g=rX,o="
        recurse=yes

- name: Create if not exists and change ownership of Zookeeper data directory.
  file: path="{{ zookeeper_data_dir }}"
        state=directory
        owner="{{ zookeeper_user }}"
        group="{{ zookeeper_group }}"
        mode="u=rwX,g=rX,o="
        recurse=yes

- name: Download Zookeeper version
  get_url: url={{ zookeeper_url }}
           dest={{ zookeeper_download_dir }}/zookeeper-{{ zookeeper_version }}.tar.gz
           owner="{{ zookeeper_user }}"
           group="{{ zookeeper_group }}"
           mode="u=rwX,g=rX,o="
           timeout=100
  register: get_url_result
  until: get_url_result.state == 'file'
  retries: 5
  delay: 1

- name: Unpack tarball
  command: tar xzf {{ zookeeper_download_dir }}/zookeeper-{{ zookeeper_version }}.tar.gz --strip-components=1
  args:
    chdir: "{{ zookeeper_dir }}"
    creates: "{{ zookeeper_dir }}/bin"

- name: Create if not exists and change ownership of Zookeeper directory.
  file: path="{{ zookeeper_dir }}"
        state=directory
        owner="{{ zookeeper_user }}"
        group="{{ zookeeper_group }}"
        mode="u=rwX,g=rX,o="
        recurse=yes

- name: Setup Apache ZooKeeper service
  template:
    src=zookeeper.service.j2
    dest="/etc/systemd/system/zookeeper.service"
    mode="u=rw,g=r,o="
  register: zookeeper_service

- name: reload systemd
  command: /usr/bin/systemctl --system daemon-reload
  when: zookeeper_service | changed
